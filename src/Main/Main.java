package Main;

import javax.swing.*;
import java.awt.*;

/**
 * Created by claudinei on 19/08/16.
 */
public class Main {

    public static void main(String [] args){
        JFrame telaPrincipal = new JFrame();

        MyCanvas myCanvas = new MyCanvas();
        MenuLateral menuLateral = new MenuLateral(myCanvas);

        telaPrincipal.setSize(1000, 1000);
        telaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        telaPrincipal.setLocationRelativeTo(null);


        telaPrincipal.add(menuLateral, BorderLayout.WEST);
        telaPrincipal.add(myCanvas, BorderLayout.EAST);

        telaPrincipal.setVisible(true);


    }

}
