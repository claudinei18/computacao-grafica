package Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by claudinei on 19/08/16.
 */
public class MenuLateral extends JPanel{
    private JButton bTranslacao;
    private JButton bRotacao;
    private JButton bEscala;
    private JButton bDDA;

    public MenuLateral(MyCanvas myCanvas){

        setBackground(Color.gray);

        BoxLayout boxlayout = new BoxLayout(this, BoxLayout.Y_AXIS);

        setLayout(boxlayout);


        bDDA = new JButton();
        bDDA.setText("Desenhar DDA");
        bDDA.setBounds(10, 100, 100, 50);

        bDDA.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getX(), y = e.getY();
                System.out.println("DDA SELECIONADO");
                myCanvas.DDA();
            }

        });

        add(bDDA);


        bEscala = new JButton();
        bEscala.setText("Escala");
        bEscala.setBounds(10, 100, 100, 50);
        add(bEscala);

        bTranslacao = new JButton();
        bTranslacao.setText("Translacao");
        bTranslacao.setBounds(10, 10, 0, 0);
        add(bTranslacao);

        bRotacao = new JButton();
        bRotacao.setText("Rotacao");
        bRotacao.setBounds(10, 50, 100, 50);
        add(bRotacao);

        bEscala = new JButton();
        bEscala.setText("Escala");
        bEscala.setBounds(10, 100, 100, 50);
        add(bEscala);
    }


}
