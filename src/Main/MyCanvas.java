package Main;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by claudinei on 19/08/16.
 */
public class MyCanvas extends Canvas {
    Point p;
    int x1, y1, x2, y2;
    boolean firstExecution;


    public void drawPoint(int x, int y){
        Graphics g = getGraphics();
        g.drawLine(x, y, x, y);
    }

    public void DDA(){
        float x = x1, y = y1;
        int dx = x2 - x1, dy = y2 - y1;
        int passos;
        float xincr, yincr;

        if(Math.abs(dx) > Math.abs(dy)){ passos = Math.abs(dx);}
        else{ passos = Math.abs(dy); }

        xincr = dx/(float)passos;
        yincr = dy/(float)passos;

        drawPoint(Math.round(x), Math.round(y));

        for(int k = 1; k<=passos; k++){
            x+=xincr;
            y+=yincr;
            drawPoint(Math.round(x), Math.round(y));
        }
}


    public MyCanvas(){
        firstExecution = true;

        Dimension d = new Dimension(700, 1000);
        setPreferredSize(d);
        setBackground(Color.WHITE);

        x1 = y1 = x2 = y2 = -1;

        addMouseListener(createListener());

    }

    private MouseListener createListener(){
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                p = new Point(e.getX(), e.getY());
                if(firstExecution){
                    x2 = p.x;
                    y2 = p.y;
                    firstExecution = false;
                }else{
                    x1 = x2;
                    y1 = y2;
                    x2 = p.x;
                    y2 = p.y;
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
    }
}
